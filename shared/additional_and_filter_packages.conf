filter_packages = [
     ("^(BaseOS|AppStream|CRB|HighAvailability|ResilientStorage|SAP|SAPHANA)$", {
         "*": [
             "kernel-rt*", #RhBug 1973568
             "javapackages-bootstrap", #CS-636
         ]
     }),

     ("^.*$", {
         "*": [
             "glibc32",
             "libgcc32",
             "scap-security-guide-rule-playbooks",
             "*openh264*",      # https://fedoraproject.org/wiki/Non-distributable-rpms
             "python3-openipmi", #RhBug 1982794
             "OpenIPMI-perl", #RhBug 1982794
         ]
         "ppc64le": [
             "SLOF",
             "guestfs-tools",
             "libguestfs",
             "libvirt-daemon-kvm",
             "libvirt-daemon-driver-qemu",
             "qemu-kiwi",
             "qemu-kvm",
             "supermin",
             "virt-manager",
             "virt-v2v",
             "virt-p2v",
             "virt-top",
             "cockpit-machines",
         ],
       "s390x": [
             "rust-std-static-wasm32-unknown-unknown", #ENGCMP-1255
             "rust-std-static-wasm32-wasi",
         ],
     }),

]


additional_packages = [
    # Everything contains everything.
    ('^Everything$', {
        '*': [
            '*',
        ],
    }),
    ("^BaseOS$", {
        "*": [
            "liblockfile", #ENGCMP-2535
            "openssl-fips-provider", #ENGCMP-3815
            "python3-gobject-base-noarch", #ENGCMP-2400
            "python3-samba-dc", #ENGCMP-3007
            "python3.9-debuginfo", #ENGCMP-1433, ENGCMP-2994
            "rhel-net-naming-sysattrs", #ENGCMP-3784
            "samba-tools", #ENGCMP-3007
            "samba-usershares", #ENGCMP-3007
            "sssd-passkey", #ENGCMP-3467
            "subscription-manager-cockpit", #ENGCMP-2427
            "subscription-manager-rhsm-certificates", #ENGCMP-2357
            "kernel-modules-core", #ENGCMP-2899
            "kernel-debug-modules-core", #ENGCMP-2899
            "ledmon-libs", #ENGCMP-4059
        ]
    }),
    ("^BaseOS$", {
         "aarch64": [
            "kernel-64k", #ENGCMP-2800
            "kernel-64k-core", #ENGCMP-2800
            "kernel-64k-debug", #ENGCMP-2800
            "kernel-64k-debug-core", #ENGCMP-2800
            "kernel-64k-debug-modules", #ENGCMP-2800
            "kernel-64k-debug-modules-core", #ENGCMP-2899, ENGCMP-2800
            "kernel-64k-debug-modules-extra", #ENGCMP-2800
            "kernel-64k-modules", #ENGCMP-2800
            "kernel-64k-modules-extra", #ENGCMP-2800
            "kernel-64k-modules-core", #ENGCMP-2899, ENGCMP-2800
         ],
         "ppc64le": [
         ],
         "x86_64": [
            "kernel-uki-virt", #ENGCMP-2899
            "kernel-debug-uki-virt", #ENGCMP-2899
         ],
         "s390x": [
            "kernel-zfcpdump-modules-core",
            "libibverbs-utils", #ENGCMP-3507
         ],
     }),
    ("^AppStream$", {
        "*": [
            "aardvark-dns", #ENGCMP-2515
            "adobe-source-code-pro-fonts", #ENGCMP-2390
            "alsa-plugins-pulseaudio", #ENGCMP-2359
            "ansible-freeipa-collection", #ENGCMP-4138
            "aspnetcore-runtime-7.0", #ENGCMP-2586
            "aspnetcore-runtime-8.0", #ENGCMP-3375
            "aspnetcore-targeting-pack-7.0", #ENGCMP-2586
            "aspnetcore-targeting-pack-8.0", #ENGCMP-3375
            "autoconf-latest", #ENGCMP-3691
            "bootc", #ENGCMP-3622
            "capstone", #ENGCMP-2591
            "cepces", #ENGCMP-3552
            "cepces-certmonger", #ENGCMP-3790
            "cepces-selinux", #ENGCMP-3803
            "cockpit-ostree", #ENGCMP-3233
            "composefs", #ENGCMP-3802
            "dotnet-apphost-pack-7.0", #ENGCMP-2586
            "dotnet-apphost-pack-8.0", #ENGCMP-3375
            "dotnet-host", #ENGCMP-2586
            "dotnet-hostfxr-7.0", #ENGCMP-2586
            "dotnet-hostfxr-8.0", #ENGCMP-3375
            "dotnet-runtime-7.0", #ENGCMP-2586
            "dotnet-runtime-8.0", #ENGCMP-3375
            "dotnet-sdk-7.0", #ENGCMP-2586
            "dotnet-sdk-8.0", #ENGCMP-3375
            "dotnet-targeting-pack-7.0", #ENGCMP-2586
            "dotnet-targeting-pack-8.0", #ENGCMP-3375
            "dotnet-templates-7.0", #ENGCMP-2586
            "dotnet-templates-8.0", #ENGCMP-3375
            "drgn", #ENGCMP-3490
            "dpdk-devel", #ENGCMP-3958
            "ecj", #ENGCMP-2928
            "efs-utils", #ENGCMP-3422
            "efs-utils-selinux", #ENGCMP-3685
            "egl-utils", #ENGCMP-2476
            "evolution-data-server-ui", #ENGCMP-3478
            "evolution-data-server-ui-devel", #ENGCMP-3477
            "fido2-tools", #ENGCMP-3526
            "firefox-x11", #ENGCMP-2806
            "freeglut-devel", #ENGCMP-2073
            "frr-selinux", #ENGCMP-2697
            "gcc-toolset-12", #ENGCMP-2391
            "gcc-toolset-12-annobin-annocheck", #ENGCMP-2384
            "gcc-toolset-12-annobin-docs", #ENGCMP-2384
            "gcc-toolset-12-annobin-plugin-gcc", #ENGCMP-2384
            "gcc-toolset-12-binutils", #ENGCMP-2415
            "gcc-toolset-12-binutils-devel", #ENGCMP-2415
            "gcc-toolset-12-binutils-gold", #ENGCMP-2415
            "gcc-toolset-12-build", #ENGCMP-2391
            "gcc-toolset-12-dwz", #ENGCMP-2402
            "gcc-toolset-12-gcc", #ENGCMP-2405
            "gcc-toolset-12-gcc-c++", #ENGCMP-2405
            "gcc-toolset-12-gcc-gfortran", #ENGCMP-2405
            "gcc-toolset-12-gcc-plugin-annobin", #ENGCMP-2805
            "gcc-toolset-12-gcc-plugin-devel", #ENGCMP-2405
            "gcc-toolset-12-gdb", #ENGCMP-2416
            "gcc-toolset-12-gdbserver", #ENGCMP-2416
            "gcc-toolset-12-libasan-devel", #ENGCMP-2405
            "gcc-toolset-12-libatomic-devel", #ENGCMP-2405
            "gcc-toolset-12-libgccjit", #ENGCMP-2405
            "gcc-toolset-12-libgccjit-devel", #ENGCMP-2405
            "gcc-toolset-12-libgccjit-docs", #ENGCMP-2405
            "gcc-toolset-12-libitm-devel", #ENGCMP-2405
            "gcc-toolset-12-liblsan-devel", #ENGCMP-2405
            "gcc-toolset-12-libquadmath-devel", #ENGCMP-2405
            "gcc-toolset-12-libstdc++-devel", #ENGCMP-2405
            "gcc-toolset-12-libstdc++-docs", #ENGCMP-2405
            "gcc-toolset-12-libtsan-devel", #ENGCMP-2405
            "gcc-toolset-12-libubsan-devel", #ENGCMP-2405
            "gcc-toolset-12-offload-nvptx", #ENGCMP-2405
            "gcc-toolset-12-runtime", #ENGCMP-2391
            "gcc-toolset-13", #ENGCMP-3157
            "gcc-toolset-13-annobin-annocheck", #ENGCMP-3279
            "gcc-toolset-13-annobin-docs", #ENGCMP-3279
            "gcc-toolset-13-annobin-plugin-gcc", #ENGCMP-3279
            "gcc-toolset-13-binutils", #ENGCMP-3146
            "gcc-toolset-13-binutils-devel", #ENGCMP-3279
            "gcc-toolset-13-binutils-gold", #ENGCMP-3279
            "gcc-toolset-13-build", #ENGCMP-3279
            "gcc-toolset-13-dwz", #ENGCMP-3142
            "gcc-toolset-13-gcc", #ENGCMP-3267
            "gcc-toolset-13-gcc-c++", #ENGCMP-3296
            "gcc-toolset-13-gcc-gfortran", #ENGCMP-3279
            "gcc-toolset-13-gcc-plugin-annobin", #ENGCMP-3279
            "gcc-toolset-13-gcc-plugin-devel", #ENGCMP-3279
            "gcc-toolset-13-gdb", #ENGCMP-3129
            "gcc-toolset-13-gdbserver", #ENGCMP-3279
            "gcc-toolset-13-libasan-devel", #ENGCMP-3279
            "gcc-toolset-13-libatomic-devel", #ENGCMP-3279
            "gcc-toolset-13-libgccjit", #ENGCMP-3279
            "gcc-toolset-13-libgccjit-devel", #ENGCMP-3279
            "gcc-toolset-13-libgccjit-docs", #ENGCMP-3279
            "gcc-toolset-13-libitm-devel", #ENGCMP-3279
            "gcc-toolset-13-liblsan-devel", #ENGCMP-3279
            "gcc-toolset-13-libquadmath-devel", #ENGCMP-3279
            "gcc-toolset-13-libstdc++-devel", #ENGCMP-3279
            "gcc-toolset-13-libstdc++-docs", #ENGCMP-3279
            "gcc-toolset-13-libtsan-devel", #ENGCMP-3279
            "gcc-toolset-13-libubsan-devel", #ENGCMP-3279
            "gcc-toolset-13-offload-nvptx", #ENGCMP-3279
            "gcc-toolset-13-runtime", #ENGCMP-3279
            "gcc-toolset-14", #ENGCMP-3903
            "gcc-toolset-14-annobin-annocheck", #ENGCMP-3904
            "gcc-toolset-14-annobin-docs", #ENGCMP-3904
            "gcc-toolset-14-annobin-plugin-gcc", #ENGCMP-3904
            "gcc-toolset-14-binutils", #ENGCMP-3905
            "gcc-toolset-14-binutils-devel", #ENGCMP-3905
            "gcc-toolset-14-binutils-gold", #ENGCMP-3905
            "gcc-toolset-14-binutils-gprofng", #ENGCMP-3905
            "gcc-toolset-14-build", 
            "gcc-toolset-14-dwz", #ENGCMP-3906
            "gcc-toolset-14-gcc", #ENGCMP-3906
            "gcc-toolset-14-gcc-c++", #ENGCMP-3906
            "gcc-toolset-14-gcc-gfortran", #ENGCMP-3906
            "gcc-toolset-14-gcc-plugin-annobin", #ENGCMP-3906
            "gcc-toolset-14-gcc-plugin-devel", #ENGCMP-3906
            "gcc-toolset-14-runtime", #ENGCMP-3903
            "gnome-kiosk-script-session", #ENGCMP-2499
            "gnome-kiosk-search-appliance", #ENGCMP-2499
            "gnome-shell-extension-custom-menu", #ENGCMP-3337
            "gnome-shell-extension-dash-to-panel", #ENGCMP-3541
            "google-crosextra-caladea-fonts", #ENGCMP-3140
            "gpsd-minimal", #ENGCMP-3333
            "gpsd-minimal-clients", #ENGCMP-3333
            "grafana-selinux", #ENGCMP-3571
            "graphviz-ruby", #ENGCMP-3570
            "gstreamer1-plugins-base-tools", #ENGCMP-2907
            "gstreamer1-rtsp-server", #ENGCMP-3178
            "gvisor-tap-vsock", #ENGCMP-3491
            "idm-pki-est", #ENGCMP-2798
            "ignition-edge", #ENGCMP-2770
            "ignition-validate", #ENGCMP-2656
            "intel-lpmd", #ENGCMP-3806
            "java-21-openjdk", #ENGCMP-3350
            "java-21-openjdk-demo", #ENGCMP-3350
            "java-21-openjdk-devel", #ENGCMP-3350
            "java-21-openjdk-headless", #ENGCMP-3350
            "java-21-openjdk-javadoc-zip", #ENGCMP-3350
            "java-21-openjdk-javadoc", #ENGCMP-3350
            "java-21-openjdk-jmods", #ENGCMP-3350
            "java-21-openjdk-src", #ENGCMP-3350
            "java-21-openjdk-static-libs", #ENGCMP-3350
            "jaxb-runtime", #ENGCMP-2881
            "jaxb-xjc", #ENGCMP-2881
            "keylime", #ENGCMP-2419
            "keylime-agent-rust", #ENGCMP-2420
            "keylime-base", #ENGCMP-2419
            "keylime-registrar", #ENGCMP-2419
            "keylime-selinux", #CS-1194
            "keylime-tenant", #ENGCMP-2419
            "keylime-verifier", #ENGCMP-2419
            "ktls-utils", #ENGCMP-3890
            "libarchive-devel", #ENGCMP-4148
            "libadwaita", #ENGCMP-3651
            "libasan8", #ENGCMP-2405
            "libblkio", #ENGCMP-3149
            "libgpiod", #ENGCMP-2433
            "libgpiod-devel", #ENGCMP-2433
            "libgpiod-utils", #ENGCMP-2433
            "libi2cd", #ENGCMP-2428
            "libi2cd-devel", #ENGCMP-2428
            "libkdumpfile", #ENGCMP-3470
            "libnxz", #ENGCMP-2576
            "libsepol-utils", #ENGCMP-2399
            "libtsan2", #ENGCMP-2405
            "libvirt-daemon-common", # ENGCMP-3046
            "libvirt-daemon-lock", # ENGCMP-3046
            "libvirt-daemon-log", # ENGCMP-3046
            "libvirt-daemon-plugin-lockd", # ENGCMP-3046
            "libvirt-daemon-proxy", # ENGCMP-3046
            "libvma-utils", #ENGCMP-3384
            "libxcvt", #ENGCMP-2791 CS-1322
            "libzdnn", #ENGCMP-2244
            "libzdnn-devel", #ENGCMP-2297
            "libzip-tools", #ENGCMP-3658
            "man-db-cron", #ENGCMP-2595
            "mingw-qemu-ga-win", #ENGCMP-3177
            "mkpasswd", #ENGCMP-2259
            "mpdecimal", #ENGCMP-2828
            "netavark", #ENGCMP-2543
            "netstandard-targeting-pack-2.1", #ENGCMP-2586
            "nfsv4-client-utils", #ENGCMP-2493
            "nvme-stas", #ENGCMP-2495
            "libnvme", #ENGCMP-2358
            "osbuild-depsolve-dnf", #ENGCMP-3857
            "passt", #ENGCMP-2741
            "passt-selinux", # ENGCMP-3074
            "pcp", #ENGCMP-3929
            "pcp-geolocate", #ENGCMP-3576
            "pcp-pmda-farm", #ENGCMP-3576
            "pf-bb-config", #ENGCMP-2857
            "pipewire-jack-audio-connection-kit-libs", #ENGCMP-3662
            "pipewire-module-x11", #ENGCMP-3336
            "poppler-qt5", #ENGCMP-2393
            "postfix-lmdb", #ENGCMP-3295
            "pyproject-srpm-macros", #ENGCMP-2964
            "python3-awscrt", #ENGCMP-3828
            "python3-botocore", #ENGCMP-3400
            "python3-dnf-plugin-leaves", #ENGCMP-3213
            "python3-dnf-plugin-show-leaves", #ENGCMP-3213
            "python3-pefile", #ENGCMP-3231
            "python3.11", #ENGCMP-2833
            "python3.11-cffi", #ENGCMP-2957
            "python3.11-charset-normalizer", #ENGCMP-2914
            "python3.11-cryptography", #ENGCMP-2958
            "python3.11-devel", #ENGCMP-2982
            "python3.11-idna", #ENGCMP-2888
            "python3.11-lxml", #ENGCMP-2954
            "python3.11-mod_wsgi", #ENGCMP-2955
            "python3.11-numpy", #ENGCMP-2933
            "python3.11-numpy-f2py", #ENGCMP-2982
            "python3.11-pip", #ENGCMP-2858
            "python3.11-ply", #ENGCMP-2911
            "python3.11-psycopg2", #ENGCMP-2956
            "python3.11-pycparser", #ENGCMP-2923
            "python3.11-PyMySQL", #ENGCMP-2963
            "python3.11-PyMySQL+rsa", #ENGCMP-2982
            "python3.11-pysocks", #ENGCMP-2912
            "python3.11-pyyaml", #ENGCMP-2910
            "python3.11-requests", #ENGCMP-2940
            "python3.11-requests+security", #ENGCMP-2982
            "python3.11-requests+socks", #ENGCMP-2982
            "python3.11-scipy", #ENGCMP-2979
            "python3.11-setuptools", #ENGCMP-2860
            "python3.11-six", #ENGCMP-2872
            "python3.11-tkinter", #ENGCMP-2982
            "python3.11-wheel", #ENGCMP-2873
            "python3-dnf-plugin-modulesync", #ENGCMP-2323
            "python3-alembic", #ENGCMP-2424
            "python3-greenlet", #ENGCMP-2421
            "python3-keylime", #ENGCMP-2419
            "python3-lark-parser", #ENGCMP-2422
            "python3-lasso", #ENGCMP-2742
            "python3-i2c-tools", #RHBZ#2072719
            "python3-libgpiod", #ENGCMP-2433
            "python3-libnvme", #ENGCMP-2412
            "python3-prompt-toolkit", #ENGCMP-3785
            "python3-pyqt5-sip", #ENGCMP-2370
            "python3-ruamel-yaml-clib", #ENGCMP-3787
            "python3-sqlalchemy", #ENGCMP-2423
            "python3-tomli", #ENGCMP-3044 CS-1485
            "python3-tornado", #ENGCMP-2418
            "python3.11-urllib3", #ENGCMP-2932
            "python3-virt-firmware", #ENGCMP-2726
            "python3-wcwidth", #ENGCMP-2093
            "python3-websockets", #ENGCMP-3788
            "python3.12", #ENGCMP-3721
            "python3.12-PyMySQL", #ENGCMP-3734
            "python3.12-PyMySQL+rsa", #ENGCMP-3734
            "python3.12-cffi", #ENGCMP-3733
            "python3.12-charset-normalizer", #ENGCMP-3735
            "python3.12-cryptography", #ENGCMP-3768
            "python3.12-devel", #ENGCMP-3721
            "python3.12-idna", #ENGCMP-3740
            "python3.12-lxml", #ENGCMP-3741
            "python3.12-mod_wsgi", #ENGCMP-3742
            "python3.12-numpy", #ENGCMP-3748
            "python3.12-numpy-f2py", #ENGCMP-3748
            "python3.12-pip", #ENGCMP-3729
            "python3.12-ply", #ENGCMP-3749
            "python3.12-psycopg2", #ENGCMP-3755
            "python3.12-pycparser", #ENGCMP-3751
            "python3.12-pyyaml", #ENGCMP-3767
            "python3.12-requests", #ENGCMP-3756
            "python3.12-scipy", #ENGCMP-3771
            "python3.12-setuptools", #ENGCMP-3670
            "python3.12-tkinter", #ENGCMP-3721
            "python3.12-urllib3", #ENGCMP-3762
            "python3.12-wheel", #ENGCMP-3671
            "qatlib-service", #ENGCMP-2490
            "redhat-cloud-client-configuration", #ENGCMP-2401
            "rtla", #ENGCMP-2799
            "rust-analyzer", #ENGCMP-2839
            "rv", #ENGCMP-3263
            "s390utils-se-data", #ENGCMP-3789
            "samba-gpupdate", #ENGCMP-3659, ENGCMP-3720
            "sip6", #ENGCMP-2239
            "sssd-idp", #ENGCMP-2276
            "stratisd-tools", #ENGCMP-3210
            "synce4l", #ENGCMP-2794
            "system-backgrounds",
            "tomcat", #ENGCMP-2927
            "tomcat-admin-webapps", #ENGCMP-2927
            "tomcat-docs-webapp", #ENGCMP-2927
            "tomcat-el-3.0-api", #ENGCMP-2927
            "tomcat-jsp-2.3-api", #ENGCMP-2927
            "tomcat-lib", #ENGCMP-2927
            "tomcat-servlet-4.0-api", #ENGCMP-2927
            "tomcat-webapps", #ENGCMP-2927
            "tuned-profiles-postgresql", #ENGCMP-2126
            "uki-direct", #ENGCMP-3698
            "usbredir-server", #ENGCMP-2719
            "vulkan-volk-devel", #ENGCMP-3668
            "xcb-util-cursor", #ENGCMP-3426
            "xcb-util-cursor-devel", #ENGCMP-3517
            "xdg-desktop-portal-gnome", #ENGCMP-2146
            "xmlstarlet", #ENGCMP-2296
            "xxhash", #ENGCMP-2455
            "xxhash-libs", #ENGCMP-2455
            "yara", #ENGCMP-2372
            "yggdrasil", #ENGCMP-3928
            "pgvector", #ENGCMP-3980
            "python3-colorama", #ENGCMP-4016
            "insights-client-ros", #ENGCMP-3824
            "systemd-ukify", #ENGCMP-3902
            "nbdkit-selinux", #ENGCMP-4006
            "pcp-export-pcp2openmetrics", #ENGCMP-3929
            "pcp-pmda-uwsgi", #ENGCMP-3929
            "lldpd-devel", #ENGCMP-4027
            "389-ds-base-snmp", #ENGCMP-4038
            "gcc-toolset-14-gdb", #ENGCMP-3954
            "gcc-toolset-14-libasan-devel", #ENGCMP-3954
            "gcc-toolset-14-libatomic-devel", #ENGCMP-3954
            "gcc-toolset-14-libgccjit-devel", #ENGCMP-3954
            "gcc-toolset-14-libgccjit-docs", #ENGCMP-3954
            "gcc-toolset-14-libgccjit", #ENGCMP-3954
            "gcc-toolset-14-libitm-devel", #ENGCMP-3954
            "gcc-toolset-14-liblsan-devel", #ENGCMP-3954
            "gcc-toolset-14-libstdc++-devel", #ENGCMP-3954
            "gcc-toolset-14-libstdc++-docs", #ENGCMP-3954
            "gcc-toolset-14-libtsan-devel", #ENGCMP-3954
            "gcc-toolset-14-libubsan-devel", #ENGCMP-3954
            "gcc-toolset-14-offload-nvptx", #ENGCMP-3954
            "openscap-report", #ENGCMP-4097
            "libkdumpfile-devel", #ENGCMP-4160
            "aspnetcore-runtime-dbg-8.0", #ENGCMP-4088
            "dotnet-runtime-dbg-8.0", #ENGCMP-4088
            "dotnet-sdk-dbg-8.0", #ENGCMP-4088
            "cockpit-files", #ENGCMP-4206
        ]
    }),
    ("^AppStream$", {
         "x86_64": [
             "awscli2", #ENGCMP-4025
             "cxl-cli", #ENGCMP-2743
             "cxl-libs", #ENGCMP-2743
             "libreoffice", #ENGCMP-2968
             "open-vm-tools-salt-minion", #ENGCMP-2295
             "pcp-pmda-resctrl", #ENGCMP-3576
             "virt-dib",
             "vorbis-tools",
             "s390utils", #ENGCMP-3606
         ],
         "aarch64": [
             "awscli2", #ENGCMP-4025
             "virt-dib",
             "kernel-64k-debug-devel", #ENGCMP-2800
             "kernel-64k-debug-devel-matched", #ENGCMP-2800
             "kernel-64k-devel", #ENGCMP-2800
             "kernel-64k-devel-matched", #ENGCMP-2800
             "rhc-worker-playbook", #ENGCMP-3127
             "s390utils", #ENGCMP-3606
         ],
         "s390x": [
             "libzpc", #ENGCMP-2756
             "virt-dib",
             "libbpf-tools", #ENGCMP-3842
         ],
         "ppc64le": [
             "vorbis-tools",
             "libreoffice", #ENGCMP-2968
             "s390utils", #ENGCMP-3606
         ],

     }),
    ("^CRB$", {
        "*": [
         ],
        "x86_64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "aarch64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "s390x": [
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "ppc64le": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
         ],
     }),

    ("^SAP$", {
        "*": [
            "compat-sap-c++-12", # ENGCMP-2843
            "compat-sap-c++-13", #ENGCMP-3991
            "compat-sap-c++-14", #ENGCMP-3954
        ]
    }),

    ("^SAPHANA$", {
        "*": [
            "compat-sap-c++-12", # ENGCMP-2843
            "compat-sap-c++-13", #ENGCMP-3953
            "compat-sap-c++-14", #ENGCMP-3954
        ]
    }),

    ("^Buildroot$", {
        "*": [
            "*",
        ]
    }),
]
