from variables import *

image_build = {
    "^BaseOS$": [
        {
            "image-build": {
                    "format": [("docker", "tar.xz")],
                    "name": "CentOS-Stream-Container-Base",
                    "target": "c9s-containers",
                    "version": "9",
                    "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                    "kickstart": "CentOS-Stream-9-container-base.ks",
                    "ksversion": "F28",
                    "distro": "Fedora-28",
                    "disk_size": 10,
                    "repo": ["BaseOS","AppStream",],
                    "subvariant": "container-base",
                    "install_tree_from": "BaseOS",
                    "arches": ["aarch64", "ppc64le", "s390x", "x86_64"],
                    "failable": ["aarch64", "ppc64le", "s390x", "x86_64"]
                    },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                    "format": [("docker", "tar.xz")],
                    "name": "CentOS-Stream-Container-Minimal",
                    "target": "c9s-containers",
                    "version": "9",
                    "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                    "kickstart": "CentOS-Stream-9-container-minimal.ks",
                    "ksversion": "F28",
                    "distro": "Fedora-28",
                    "disk_size": 10,
                    "repo": ["BaseOS","AppStream",],
                    "subvariant": "container-minimal",
                    "install_tree_from": "BaseOS",
                    "arches": ["aarch64", "ppc64le", "s390x", "x86_64"],
                    "failable": ["aarch64", "ppc64le", "s390x", "x86_64"]
                    },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                "format": [("qcow2", "qcow2")],
                "name": "CentOS-Stream-GenericCloud",
                "target": "c9s-images",
                "version": "9",
                "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                "kickstart": "CentOS-Stream-9-kvm.ks",
                "ksversion": "F26",
                "distro": "Fedora-20",
                "disk-size": "10",
                "repo": ["BaseOS","AppStream",],
                "subvariant": "generic",
                "install_tree_from": "BaseOS",
                "arches": ["aarch64", "ppc64le", "s390x", "x86_64"],
                "failable": ["aarch64", "ppc64le", "s390x", "x86_64"],
                "scratch": IMAGE_BUILD_SCRATCH
            },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                "format": [("qcow2", "qcow2")],
                "name": "CentOS-Stream-GenericCloud-x86_64",
                "target": "c9s-images",
                "version": "9",
                "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                "kickstart": "CentOS-Stream-9-kvm-x86_64.ks",
                "ksversion": "F26",
                "distro": "Fedora-20",
                "disk-size": "10",
                "repo": ["BaseOS","AppStream",],
                "subvariant": "generic-x86_64",
                "install_tree_from": "BaseOS",
                "arches": ["x86_64"],
                "failable": ["x86_64"],
                "scratch": IMAGE_BUILD_SCRATCH
            },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                "format": [("raw-xz", "raw.xz")],
                "name": "CentOS-Stream-ec2",
                "target": "c9s-images",
                "version": "9",
                "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                "kickstart": "CentOS-Stream-9-ec2.ks",
                "ksversion": "F26",
                "distro": "Fedora-20",
                "disk-size": "10",
                "repo": ["BaseOS","AppStream",],
                "subvariant": "ec2",
                "install_tree_from": "BaseOS",
                "arches": ["x86_64"],
                "failable": ["x86_64"],
                "scratch": IMAGE_BUILD_SCRATCH
            },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                "format": [("raw-xz", "raw.xz")],
                "name": "CentOS-Stream-ec2-aarch64",
                "target": "c9s-images",
                "version": "9",
                "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                "kickstart": "CentOS-Stream-9-ec2-aarch64.ks",
                "ksversion": "F26",
                "distro": "Fedora-20",
                "disk-size": "10",
                "repo": ["BaseOS","AppStream",],
                "subvariant": "ec2-aarch64",
                "install_tree_from": "BaseOS",
                "arches": ["aarch64"],
                "failable": ["aarch64"],
                "scratch": IMAGE_BUILD_SCRATCH
            },
            "factory-parameters": {
                "generate_icicle": False,
            },
        },
        {
            "image-build": {
                "format": [("vagrant-libvirt", "vagrant-libvirt.box"),('vagrant-virtualbox','vagrant-virtualbox.box')],
                "name": "CentOS-Stream-Vagrant",
                "version": "9",
                "target": "c9s-images",
                "ksurl": "git+https://gitlab.com/redhat/centos-stream/release-engineering/kickstarts.git?#HEAD",
                "kickstart": "CentOS-9-Stream-x86_64-Vagrant.ks",
                "ksversion": "F26",
                "distro": "Fedora-20",
                "disk-size": "10",
                "repo": ["BaseOS","AppStream"],
                "subvariant": "vagrant",
                "install_tree_from": "BaseOS",
                "arches": ["x86_64"],
                "failable": ["x86_64"],
                "scratch": IMAGE_BUILD_SCRATCH
            },
            "factory-parameters": {
                "generate_icicle": False,
            }
        },
    ],
}
